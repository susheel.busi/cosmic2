#!/bin/bash -l

# USAGE: sbatch -p batch -t 01:00:00 -N 1 -n 2 get_HGT_contig_ID.sh
# Used to retrieve the bins and contigs that underwent HORIZONTAL GENE TRANSFER
# Subsequently recovers the MetaCHIP contigID and the "ORIGINAL" contigID
# Also retrieves the location of the contigs from the ".gbk" file

cd /mnt/isilon/projects/ecosystem_biology/AMR_2020/mice_AMR_analyses/mice_AMR/hgt/metachip

#########
# Extracting ALL HGT contigs from all bins from within the MetaCHIP output
#########
# getting all the individual HGT bins for each sample
# getting a list of all the HGT events in all samples
for sample in `cat samples_list`
do
    file=$(find "$sample"_MetaCHIP_wd -name '*_g_detected_HGTs.txt')
#    echo $(dirname $file | awk -F "/" '{print $1}')
    cat $file | sed '/^Gene/d' > "$sample"_detected_HGTs
    awk '{ for (i=1; i<=2; i++) print $i }' "$sample"_detected_HGTs | sort | uniq > "$sample"_all_contigs     # writing the two columns below each other and keeping only contigs
    cat "$sample"_all_contigs | sed 's/_.*//' | sort | uniq > "$sample"_all_bins          # removing contigIDs and keeping bin names
done

# recovering the respective contigs involved in HGT and their locations in the assembly from the `genbank` file
for sample in `cat samples_list`
do
    for i in $(cat "$sample"_all_bins | sed 's/\_.*//')
    do
      for x in $(cat "$sample"_all_contigs)
      do
        gbk=$(find "$sample"_MetaCHIP_wd -name "$i.gbk")
        grep -B 1 "$x" $gbk
      done
    done | paste - - > "$sample"_all_locations
done

# finding a match for locus_tag and then finding another match before that for `contig_ID`
# Example:
# awk -v valtofind="tx-145_00002" '
# /^LOCUS/{
#   id=$2
#   next
# }
# /\/locus_tag/ && $0 ~ "\""valtofind"\"$" {
#   print id,valtofind
#   id=""
# }
# '  Input_file

# linking "ORIGINAL" contigID with the "metachip" contigID
for sample in `cat samples_list`
do
    for i in $(cat "$sample"_all_bins | sed 's/\_.*//')
    do
      for x in $(cat "$sample"_all_contigs)
      do
        gbk=$(find "$sample"_MetaCHIP_wd -name "$i.gbk")
        awk -v valtofind="$x" '
        /^LOCUS/{
        id=$2
        next
        }
        /\/locus_tag/ && $0 ~ "\""valtofind"\"$" {
        print id,valtofind
        id=""
        }
        ' $gbk
      done
    done > "$sample"_all_contig_ID
done

# merging `contig_ID` with location of the sequences
for sample in `cat samples_list`
do
  awk '{print $2"\t"$3}' "$sample"_all_locations | sed 's/\/locus_tag="//' | sed 's/"//' > "$sample"_all_tmp
  join -j 2 -o 2.2 2.1 1.1 "$sample"_all_tmp "$sample"_all_contig_ID > "$sample"_all_contig_ID_location.txt
  sed -i $'1 i\\\nbin_name\tcontig_ID\tcoordinate' "$sample"_all_contig_ID_location.txt
done

### all the "$sample"_ALL_contig_ID_location.txt files were analysed further by Laura for looking up the AMR/CRISPR locations