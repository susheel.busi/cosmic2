#!/bin/bash -l
#SBATCH -J PRJNA489090_fastq-dump
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=susheel.busi@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 24
#SBATCH --time=00-10:00:00
#SBATCH -p batch
#SBATCH --qos=qos-batch

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

export PATH=$PATH:"/mnt/isilon/projects/ecosystem_biology/NOMIS/Datasets_external/ena-fast-download-master/"

conda activate EDirect
cd /work/projects/ecosystem_biology/COSMIC/2019_COSMIC_Analysis/public_datasets/PRJNA489090
for sample in `cat /work/projects/ecosystem_biology/COSMIC/2019_COSMIC_Analysis/public_datasets/PRJNA489090_list`
do
        # fastq-dump --split-files --origfmt --gzip $sample
        # parallel-fastq-dump --sra-id "$sample" --outdir fastq/cryosphere_fastq/ --split-files --gzip -t 4
        parallel-fastq-dump --sra-id "$sample" --split-files --gzip -t 4
done

#for sample in `cat ../accession_list`
#do
#       ena-fast-download.py ERR1739691
#done

#ena-fast-download.py ERR1739691

echo "== Ending run at $(date)"
