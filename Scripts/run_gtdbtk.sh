#!/bin/bash -l
#SBATCH -J gtdbtk
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=susheel.busi@uni.lu
#SBATCH -N 1
#SBATCH -n 8
## SBATCH --mem MAX
#SBATCH --time=2-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=qos-bigmem

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

conda activate gtdbtk
gtdbtk classify_wf --cpus 8 -x fa --genome_dir /work/projects/ecosystem_biology/COSMIC/2019_COSMIC_Analysis/hgtector/external_samples --out_dir /work/projects/ecosystem_biology/COSMIC/2019_COSMIC_Analysis/hgtector/external_samples_gtdbtk_output

echo "== Ending run at $(date)"