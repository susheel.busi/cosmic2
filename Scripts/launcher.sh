#!/bin/bash

for file in `cat /work/projects/ecosystem_biology/COSMIC/2019_COSMIC_Analysis/hgtector/metachip_list`
do
        sbatch run_metachip.sh $file
done