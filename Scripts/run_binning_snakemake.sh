#!/bin/bash -l
SLURM_ARGS="-p {cluster.partition} -N 1 -n {cluster.n} -c {cluster.ncpus} -t {cluster.time} --job-name={cluster.job-name} -o {cluster.output} -e {cluster.error}"
(date; conda activate snakemake; snakemake -j 5 --use-conda --latency-wait 60 --cluster-config binning_cluster.json --cluster "sbatch $SLURM_ARGS" -s binning_snakefile -p; date) &> smk.log
