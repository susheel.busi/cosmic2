#!/bin/bash -l
#SBATCH -J metachip
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=susheel.busi@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 18
# SBATCH --mem=64GB
#SBATCH --time=01-00:00:00
#SBATCH -p bigmem
#SBATCH --qos=qos-bigmem

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

cd /work/projects/ecosystem_biology/COSMIC/2019_COSMIC_Analysis/hgtector

ml swenv/default-env/v1.1-20180716-production
ml lang/Python/3.6.4-intel-2018a
export PATH=$PATH:"~/.local/bin"
export PATH=$PATH:"~/apps/miniconda3/bin"
export PATH=$PATH:"/home/users/sbusi/apps/metachip/bin"
export PATH=$PATH:"/work/projects/ecosystem_biology/local_tools/FastTree"

NUM_THREADS=18

lib=$1

MetaCHIP PI -p "$lib" -r g -t $NUM_THREADS -i "$lib" -x fa -taxon "$lib"_gtdbtk_output/"$lib"_gtdbtk.tsv
MetaCHIP BP -p "$lib" -r g -t $NUM_THREADS -force

echo "== Ending run at $(date)"
