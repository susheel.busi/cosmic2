#!/bin/bash -l
#SBATCH -J metachip
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=susheel.busi@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 24
#SBATCH --time=00-03:00:00
#SBATCH -p bigmem
#SBATCH --qos=qos-bigmem

echo "== Starting run at $(date)"
echo "== Job ID: ${SLURM_JOBID}"
echo "== Node list: ${SLURM_NODELIST}"
echo "== Submit dir. : ${SLURM_SUBMIT_DIR}"

export PYTHONNOUSERSITE=True

cd /scratch/users/ldenies/mice_AMR/hgt/metachip

conda activate METACHIP

NUM_THREADS=24

lib=$1

MetaCHIP PI -p "$lib" -r g -t $NUM_THREADS -i "$lib" -x fa -taxon "$lib"/"$lib"_gtdbtk.tsv -tmp
MetaCHIP BP -p "$lib" -r g -t $NUM_THREADS -force -tmp

echo "== Ending run at $(date)"