#!/bin/bash -l
NUM_THREADS=24

conda activate conda_env
for i in `cat list`
do
        for f in $i.fna
        do
        bwa index $f
        bwa mem -v 1 -t $NUM_THREADS -M $f $i/mg.r1.preprocessed.fq $i/mg.r2.preprocessed.fq > $f.sam
	samtools view -bS $f.sam > $f.bam
        samtools sort $f.bam -o $f.sorted.bam	
	done
done

cond deactivate
conda activate binning

for i in `cat list`
do
        for f in $i.fna
	do
                jgi_summarize_bam_contig_depths --outputDepth ${f}_depth.txt --pairedContigs ${f}_paired.txt $f.sorted.bam
        done
done
