#!/usr/bin/env python
import os
import sys
import pandas as pd
import numpy as np

# setting the working directory
# os.chdir('/scratch/users/sbusi/cosmic_review/vibrant/VIBRANT/cosmic_vcontact2')

# reading the file
print(sys.argv[1])
df = pd.read_csv(sys.argv[1])

# summarising the columns based on a single column, i.e. getting total number of proteins based on genus names
sum_df = df.groupby(['Genus'], as_index=False)['Size'].sum()

# saving the file to .txt
filename=sys.argv[1]
#sum_df.to_csv(rsys.argv[1]+'.txt', index=None, sep='\t')
sum_df.to_csv(os.path.splitext(sys.argv[1])[0]+".txt", index=None, sep='\t')