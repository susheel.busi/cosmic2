#!/usr/bin/env python
import os
import pandas as pd
# argparse

# setting the working directory
# os.chdir('/scratch/users/sbusi/cosmic_review/vibrant/VIBRANT/kaiju_output')

# reading the file
df=pd.read_csv('kaiju_summary.out', sep='\t')

#keeping only specific columns
filt=['file', 'percent', 'taxon_name']
df=df[filt]

# fixing the columns by removing additional strings
df['file']=df['file'].str.replace('/scratch/users/sbusi/cosmic_review/vibrant/VIBRANT/kaiju_output/','')
df['file']=df['file'].str.replace('_kaiju.out','')

# pivoting the values from rows to columns
df_final=test.pivot(index='taxon_name', columns='file', values='percent')

# saving the files for later use
df_final.to_csv("kaiju_output_wide.csv")
df.to_csv("kaiju_output_long.csv")
